package com.br.authorbooks.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.br.authorbooks.model.Author;

public interface IAuthorRepository extends JpaRepository<Author, Long> {

}
