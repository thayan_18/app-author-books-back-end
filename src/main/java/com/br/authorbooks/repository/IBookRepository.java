package com.br.authorbooks.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.br.authorbooks.model.Author;
import com.br.authorbooks.model.Book;

public interface IBookRepository extends JpaRepository<Book, Long> {

	List<Book> findByAuthor(Author author);

}
