package com.br.authorbooks.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.br.authorbooks.model.Book;
import com.br.authorbooks.service.BookService;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("books")
public class BookController {

	@Autowired
	private BookService bookService;

	// getId
	@RequestMapping(method = RequestMethod.GET, value = "{id}")
	public ResponseEntity<Book> findById(@PathVariable Long id) {
		try {
			return new ResponseEntity<Book>(bookService.findById(id),
					HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Book>(HttpStatus.NO_CONTENT);
		}
	}

	// getAll
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Book>> findAll() {
		return new ResponseEntity<List<Book>>(bookService.findAll(),
				HttpStatus.OK);
	}

	// save-update
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Book> save(@RequestBody Book newBook) {
		return new ResponseEntity<Book>(bookService.save(newBook),
				HttpStatus.OK);
	}

	// remove
	@RequestMapping(method = RequestMethod.DELETE, value = "{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		try {
			bookService.delete(id);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.noContent().build();
		}
		return ResponseEntity.ok().build();
	}

	
}
