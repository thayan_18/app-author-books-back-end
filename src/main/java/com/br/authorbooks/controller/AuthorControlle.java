package com.br.authorbooks.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.br.authorbooks.model.Author;
import com.br.authorbooks.model.Book;
import com.br.authorbooks.service.AuthorService;
import com.br.authorbooks.service.BookService;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("authors")
public class AuthorControlle {

	@Autowired
	private AuthorService authorService;

	@Autowired
	private BookService bookService;

	// getId
	@RequestMapping(method = RequestMethod.GET, value = "{id}")
	public ResponseEntity<Author> findById(@PathVariable Long id) {
		try {
			return new ResponseEntity<Author>(authorService.findById(id),
					HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Author>(HttpStatus.NO_CONTENT);
		}

	}

	// getAll
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Author>> findAll() {
		return new ResponseEntity<List<Author>>(authorService.findAll(),
				HttpStatus.OK);
	}

	// save-update
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Author> save(@RequestBody Author newAuthor) {
		return new ResponseEntity<Author>(authorService.save(newAuthor),
				HttpStatus.OK);
	}

	// remove
	@RequestMapping(method = RequestMethod.DELETE, value = "{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		try {
			authorService.delete(id);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.noContent().build();
		}
		return ResponseEntity.ok().build();
	}

	// getAll
	@RequestMapping(method = RequestMethod.GET, value = "/{id}/books")
	public ResponseEntity<List<Book>> findByAuthor(@PathVariable Long id) {
		return new ResponseEntity<List<Book>>(bookService.findByAuthor(id),
				HttpStatus.OK);
	}

}
