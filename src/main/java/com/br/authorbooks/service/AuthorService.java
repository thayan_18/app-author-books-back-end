package com.br.authorbooks.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.br.authorbooks.model.Author;
import com.br.authorbooks.repository.IAuthorRepository;

@Service
@Transactional
public class AuthorService {

	@Autowired
	private IAuthorRepository authorRepository;

	public List<Author> findAll() {
		return authorRepository.findAll();
	}

	public Author findById(Long id) {
		return authorRepository.findOne(id);
	}

	public Author save(Author author) {
		return authorRepository.save(author);
	}

	public void delete(Long id) {
		authorRepository.delete(id);
	}

}
