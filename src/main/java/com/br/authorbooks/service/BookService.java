package com.br.authorbooks.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.br.authorbooks.model.Author;
import com.br.authorbooks.model.Book;
import com.br.authorbooks.repository.IBookRepository;

@Service
@Transactional
public class BookService {

	@Autowired
	private IBookRepository bookRepository;

	public List<Book> findAll() {
		return bookRepository.findAll();
	}

	public Book findById(Long id) {
		return bookRepository.findOne(id);
	}

	public Book save(Book book) {
		return bookRepository.save(book);
	}

	public void delete(Long id) {
		bookRepository.delete(id);
	}

	public List<Book> findByAuthor(Long id) {
		return bookRepository.findByAuthor(new Author(id));
	}

}
